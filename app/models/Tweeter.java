package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import play.db.jpa.GenericModel;

@Entity
public class Tweeter extends GenericModel
{
  @Id
  public String id;
  public String firstName;
  public String lastName;
  public String email;
  public String password;


 @OneToMany(cascade = CascadeType.ALL)
 // @OneToMany(mappedBy="tweeter", cascade = CascadeType.ALL)
 public List<Tweet> tweets = new ArrayList<Tweet>();

  public static Tweeter findByEmail(String email)
  {
    return find("email", email).first();
  }
  
  public static Tweeter authenticate(String email, String password)
  {
    Tweeter user = Tweeter.findByEmail(email);
    if (user != null && user.checkPassword(password))
    {
    	return user;
    }else{
    	return null;
    }
  }
  
// find by ID
  public static Tweeter findById(String id)
  {
    return find("id", id).first();
  }


  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }

  public String getFullName()
  {
    return firstName + " " + lastName;
  }

}