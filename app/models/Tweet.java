package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import play.db.jpa.GenericModel;
import play.db.jpa.Model;

@Entity
public class Tweet extends GenericModel {
	// extends GenericModel, not Model as in previous Play apps which
	// we developed (Donation, MyRent, WitPress).
	// The annotation @Id means that we, the developers, are deciding the
	// primary key,
	// the id for the database table.
	@Id
	public String id;
	public String tweetText;
	public String date;
	
	@ManyToOne
	  public Tweeter tweeter;
	
	@Override
	public Tweet delete(){
		
			tweeter.tweets.remove(this);
			tweeter.save();
			super.delete();
			return this;
		
	}
	
}
