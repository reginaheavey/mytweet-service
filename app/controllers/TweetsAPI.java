package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import models.Tweet;
import models.Tweeter;
import play.Logger;
import play.mvc.Controller;
import utils.GsonStrategy;

import java.util.ArrayList;
import java.util.List;

public class TweetsAPI extends Controller {
	static Gson gson = new GsonBuilder().setExclusionStrategies(new GsonStrategy()).create();

	/**
	 * 
	 * @param id
	 *            : intended for tweeter id in later iteration
	 * @param body
	 *            : the tweet to be created
	 */

	public static void createTweet(String id, JsonElement body) {
		Tweet tweet = gson.fromJson(body.toString(), Tweet.class);
		Tweeter tweeter = Tweeter.findById(id);
		tweeter.tweets.add(tweet);
		tweet.tweeter = tweeter;
		tweet.save();
		renderJSON(gson.toJson(tweet));
	}

	// new
	public static void getTweetsByUserId(String id) {
		Tweeter usr = Tweeter.findById(id);

		if (usr == null) {
			renderJSON(gson.toJson(new ArrayList<Tweet>()));
		} else {
			renderJSON(gson.toJson(usr.tweets));
		}
	}

	// @param id : The id of the tweeter (the tweet list owner)
	public static void getTweets(String id) {
		Tweeter tweeter = Tweeter.findById(id);
		if (tweeter == null) {
			notFound();
		}
		renderJSON(gson.toJson(tweeter.tweets));
	}

	/*
	 * @param id : The tweeter id. This is redundant here since tweet id a uuid
	 * and so unique.
	 * 
	 * @param tweetId : The id of tweet for deletion.
	 */
	public static void deleteTweet(String id, String tweetId) {
		Tweet tweet = Tweet.findById(id);
		if (tweet == null) {
			notFound();
		} else {
			//Tweeter tweeter = tweet.tweeter;
			//tweeter.tweets.remove(tweet);
			//tweeter.save();
			tweet.delete();
			renderJSON(gson.toJson(tweet));
		}
	}

	public static void getAllTweets() {

		List<Tweet> tweets = Tweet.findAll();
		renderJSON(gson.toJson(tweets));
	}

	public static void deleteAllTweets() {
		List<Tweet> tweets = Tweet.findAll();
		for(Tweet tweet : tweets){
			tweet.delete();
		}
		renderText("success");
	}

	/*
	 * @param id : The tweeter id. This is redundant here since tweet id a uuid
	 * and so unique.
	 * 
	 * @param tweetId : The id of tweet sought.
	 */
	public static void getTweet(String id, String tweetId) {
		Tweet tweet = Tweet.findById(id);
		if (tweet == null) {
			notFound();
		} else {
			renderJSON(gson.toJson(tweet));
		}
	}
}
