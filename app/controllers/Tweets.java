package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Tweets extends Controller {
	public static void index() {
		Tweeter tweeter = Accounts.getCurrentUser();
		Logger.info("Tweet controller : user is " + tweeter.email);
		List<Tweet> tweets = tweeter.tweets;
		render(tweeter, tweets);
	}

	public static void renderreport() {
		Tweeter tweeter = Accounts.getCurrentUser();
		List<Tweet> tweets = tweeter.tweets;
		render(tweets);
	}
}