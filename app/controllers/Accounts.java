package controllers;

import play.*;
import play.mvc.*;

import java.util.List;

import models.*;

public class Accounts extends Controller
{
  public static void index()
  {
	  List<Tweet> allTweets = Tweet.findAll();
	  	render(allTweets); 	
  }

  public static void login()
  {
    render();
  }

  public static void logout()
  {
    session.clear();
    index();
  }

  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" + password);

    Tweeter user = Tweeter.authenticate(email, password);
    if ((user != null))
    {
      Logger.info("Successfull authentication of  " + user.firstName + " " + user.lastName);
      session.put("logged_in_userid", user.id);
      Tweets.renderreport();
    }
    else
    {
      Logger.info("Authentication failed");
      login();
    }
  }

  public static Tweeter getCurrentUser()
  {
    String userId = session.get("logged_in_userid");
    if (userId == null)
    {
      index();
    }
    Tweeter logged_in_user = Tweeter.findById(userId);
    Logger.info("In Accounts controller: Logged in user is " + logged_in_user.firstName);
    return logged_in_user;
  }
}